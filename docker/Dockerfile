# Build stage
FROM node:20.10.0 AS build

## Create app directory
WORKDIR /app

## Install app dependencies
COPY package*.json tsconfig*.json ./
RUN npm ci --omit=dev

## Bundle app source
COPY ./src ./src

## Build app
RUN npm run build



# Run stage
FROM node:20.10.0

## Switch to less privileged user
USER node

## Declare env vars
ENV HEARTBEAT_CLIENT_ID="ede88b30-1ba0-431a-9775-acfdf2ac0f57"
ENV HEARTBEAT_CLIENT_INTERVAL=10
ENV HEARTBEAT_CLIENT_DATE_FORMAT="YYYY-MM-DD HH:mm:ss:SSS Z"
ENV HEARTBEAT_SERVER_BASE_URL="http://localhost:3000"

## Create app directory
WORKDIR /app

## Copy app
COPY --from=build /app/node_modules ./node_modules
COPY --from=build /app/dist ./dist

## Execute app
CMD [ "node", "dist/main"]